// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include "catch.hpp"
#include <Eigen/Dense>
#include <autodiff.hh>
#include <interior_point_method.hh>
#include <iostream>
#include <linear_model.hh>
using namespace Eigen;

/** Test Function
 * f(x,y) = g(x,y) + 0.1*y^2
 * AD Function: g(x,y) =  sin(log(1+x^2+y^2)^2+x^2+y^2)^2
 */
FunctionTargetResult target_compute(VectorXd x, VectorXd &coeff_vec,
                                    MatrixXd &coeff_mat,
                                    std::vector<LinearModel> &linear_models) {
  // init value, gradient, hessian
  double value = 0.0;
  VectorXd gradient = VectorXd::Zero(x.size());
  MatrixXd hessian = MatrixXd::Zero(x.size(), x.size());
  // set x for linear models
  linear_models[0].set_x(x);
  value += linear_models[0].val(0);
  gradient += linear_models[0].grad(0);
  hessian += linear_models[0].hes(0);
  // non ad function: -x(1)^2
  value += 0.1 * pow(x(1), 2);
  gradient(1) += 0.2 * x(1);
  hessian(1, 1) += 0.2;
  // build result struct
  FunctionTargetResult result;
  result.value = value;
  result.gradient = gradient;
  result.hessian = hessian;

  return result;
}
// AD Function: g(x,y) =  sin(log(1+x^2+y^2)^2+x^2+y^2)^2
AD_Decl_Function target_compute_lin_model_0(AD_Vector x) {
  return pow(
      sin(log(1 + pow(x(0), 2) + pow(x(1), 2)) + pow(x(0), 2) + pow(x(1), 2)),
      2);
}

/** Inequality Constraint: f(x) = -x^2-x^2 -0.3 <=0
 */
FunctionTargetResult
inequality_compute(VectorXd x, VectorXd &coeff_vec, MatrixXd &coeff_mat,
                   std::vector<LinearModel> &linear_models) {
  // init value, gradient, hessian
  double value = 0.0;
  VectorXd gradient = VectorXd::Zero(x.size());
  MatrixXd hessian = MatrixXd::Zero(x.size(), x.size());
  // set x for linear models
  linear_models[0].set_x(x);
  value += linear_models[0].val(0);
  gradient += linear_models[0].grad(0);
  hessian += linear_models[0].hes(0);
  FunctionTargetResult result;
  result.value = value;
  result.gradient = gradient;
  result.hessian = hessian;
  return result;
}
// AD Function: g(x,y) =  sin(log(1+x^2+y^2)^2+x^2+y^2)^2
AD_Decl_Function inequality_lin_model_0(AD_Vector x) {
  return -pow(x(0), 2) - pow(x(1), 2) + (AD_Decl_Function)0.2;
}

TEST_CASE("Interior Point Method", "[NLPSolver]") {
  FunctionTarget example_target;
  example_target.compute = target_compute;

  LinearModel lin_model;
  LinearModelFeature example_feature;
  example_feature.function = target_compute_lin_model_0;
  example_feature.id = 0;
  lin_model.add_feature(example_feature);
  example_target.linear_models.push_back(lin_model);

  NLPFunction nlp_function;
  nlp_function.add_target(example_target);

  NLPSolverOption nlp_solver_option;
  NLPMinimizeOption nlp_minimize_option;
  SECTION("global minimum at (0,0)") {
    VectorXd x0(2);
    x0(0) = 0.45;
    x0(1) = -0.45;
    nlp_minimize_option.x0 = x0;
    VectorXd x_u(2);
    x_u(0) = 0.5;
    x_u(1) = 0.5;
    VectorXd x_l(2);
    x_l(0) = -0.5;
    x_l(1) = -0.5;
    nlp_minimize_option.bounds.x.u = x_u;
    nlp_minimize_option.bounds.x.l = x_l;
    NLPSolver nlp_solver(nlp_function, nlp_solver_option);
    VectorXd global_min = VectorXd::Zero(2);
    VectorXd nlp_min = nlp_solver.minimize(nlp_minimize_option);
    VectorXd min_diff = global_min - nlp_min;
    // requirements
    REQUIRE(min_diff.norm() <= 1e-4);
  }
  SECTION("global minimum at (0.1,0.1), x,y restricted domain outer global "
          "minimum") {
    VectorXd x0(2);
    x0(0) = 0.45;
    x0(1) = 0.45;
    nlp_minimize_option.x0 = x0;
    VectorXd x_u(2);
    x_u(0) = 0.5;
    x_u(1) = 0.5;
    VectorXd x_l(2);
    x_l(0) = 0.1;
    x_l(1) = 0.1;
    nlp_minimize_option.bounds.x.u = x_u;
    nlp_minimize_option.bounds.x.l = x_l;
    VectorXd global_min(2);
    global_min(0) = 0.1;
    global_min(1) = 0.1;
    NLPSolver nlp_solver(nlp_function, nlp_solver_option);
    VectorXd nlp_min = nlp_solver.minimize(nlp_minimize_option);
    VectorXd min_diff = global_min - nlp_min;
    // requirements
    REQUIRE(min_diff.norm() <= 1e-3);
  }
  MatrixXd equality_constr_mat(1, 2);
  equality_constr_mat(0, 0) = 1.0;
  equality_constr_mat(0, 1) = 1.0;
  nlp_minimize_option.lin_equal_constr.mat = equality_constr_mat;
  SECTION("equality constraint") {
    VectorXd x0(2);
    x0(0) = 0.15;
    x0(1) = 0.46;
    nlp_minimize_option.x0 = x0;
    VectorXd x_u(2);
    x_u(0) = 0.5;
    x_u(1) = 0.5;
    VectorXd x_l(2);
    x_l(0) = 0.1;
    x_l(1) = 0.1;
    nlp_minimize_option.bounds.x.u = x_u;
    nlp_minimize_option.bounds.x.l = x_l;
    VectorXd global_min(2);
    global_min(0) = 0.1;
    global_min(1) = 0.1;
    NLPSolver nlp_solver(nlp_function, nlp_solver_option);
    VectorXd nlp_min = nlp_solver.minimize(nlp_minimize_option);
    REQUIRE(abs(nlp_min.sum() - x0.sum()) <= 1e-6);
  }

  SECTION("inequality constraint: x^2 + y^2 >= 0.2") {
    FunctionTarget example_inequality;
    example_inequality.compute = inequality_compute;

    LinearModel lin_model_inequ;
    LinearModelFeature example_feature_ineq;
    example_feature_ineq.function = inequality_lin_model_0;
    example_feature_ineq.id = 0;
    lin_model_inequ.add_feature(example_feature_ineq);
    example_inequality.linear_models.push_back(lin_model_inequ);

    NLPFunction nlp_function_inequ;
    nlp_function_inequ.add_target(example_inequality);
    nlp_minimize_option.lin_equal_constr.mat = MatrixXd::Zero(0, 0);
    nlp_minimize_option.inequal_constr.function = nlp_function_inequ;
    nlp_minimize_option.inequal_constr.use_template = "custom";
    VectorXd x0(2);
    x0(0) = 0.15;
    x0(1) = 0.46;
    nlp_minimize_option.x0 = x0;
    VectorXd x_u(2);
    x_u(0) = 0.5;
    x_u(1) = 0.5;
    VectorXd x_l(2);
    x_l(0) = 0.1;
    x_l(1) = 0.1;
    nlp_minimize_option.bounds.x.u = x_u;
    nlp_minimize_option.bounds.x.l = x_l;
    VectorXd global_min(2);
    global_min(0) = 0.1;
    global_min(1) = 0.1;
    NLPSolver nlp_solver(nlp_function, nlp_solver_option);
    VectorXd nlp_min = nlp_solver.minimize(nlp_minimize_option);
    REQUIRE(pow(nlp_min(0), 2) + pow(nlp_min(1), 2) >= 0.2);
  }
}
