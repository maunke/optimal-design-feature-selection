// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include "catch.hpp"
#include <Eigen/Dense>
#include <adaptive_grid_optimal_design.hh>
#include <autodiff.hh>
#include <interior_point_method.hh>
#include <iostream>
#include <linear_model.hh>
using namespace Eigen;

AD_Decl_Function polynom_monom(AD_Vector x, VectorXd gen) {
  return pow(x, gen);
}
// linear model 1,x,y,x^2,y^2,xy adaptive grid method
TEST_CASE("Adaptive Grid Optimal Design", "[AdapativeGridOptimalDesign]") {
  setNbThreads(1);
  VectorXd x_u(2);
  x_u(0) = 1.0;
  x_u(1) = 1.0;
  VectorXd x_l(2);
  x_l(0) = -1.0;
  x_l(1) = -1.0;
  VectorXd x_q(2);
  int q = 21;
  x_q(0) = q;
  x_q(1) = q;
  DesignMeasureGrid grid(x_l, x_u, x_q);

  LinearModel lin_model;
  VectorXd gen(2);
  LinearModelFeature example_feature;
  example_feature.function = polynom_monom;
  example_feature.id = 0;
  gen(0) = 0;
  gen(1) = 0;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);
  example_feature.id = 1;
  gen(0) = 1;
  gen(1) = 0;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);
  example_feature.id = 2;
  gen(0) = 0;
  gen(1) = 1;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);
  example_feature.id = 3;
  gen(0) = 2;
  gen(1) = 0;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);
  example_feature.id = 4;
  gen(0) = 0;
  gen(1) = 2;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);
  example_feature.id = 5;
  gen(0) = 1;
  gen(1) = 1;
  example_feature.gen = gen;
  lin_model.add_feature(example_feature);

  AdapativeGridOptimalDesignOption adaptive_grid_option;
  adaptive_grid_option.supp.minimize.bounds.x.u = x_u;
  adaptive_grid_option.supp.minimize.bounds.x.l = x_l;
  DesignOption design_option;
  design_option.design_type = "d-criterium";
  design_option.linear_models.push_back(lin_model);

  AdapativeGridOptimalDesign adaptive_grid_method(design_option,
                                                  adaptive_grid_option);
  OptimalDesign rslt = adaptive_grid_method.solve(grid.supp, false);
  SECTION("linear model 1,x,y,x^2,y^2,xy: check weights and support points") {
    // support points check
    MatrixXd check_rslt_supp = MatrixXd::Zero(2, 9);
    check_rslt_supp(0, 0) = -1;
    check_rslt_supp(1, 0) = -1;
    check_rslt_supp(1, 1) = -1;
    check_rslt_supp(0, 2) = 1;
    check_rslt_supp(1, 2) = -1;
    check_rslt_supp(0, 3) = -1;
    check_rslt_supp(0, 5) = 1;
    check_rslt_supp(0, 6) = -1;
    check_rslt_supp(1, 6) = 1;
    check_rslt_supp(1, 7) = 1;
    check_rslt_supp(0, 8) = 1;
    check_rslt_supp(1, 8) = 1;
    MatrixXd diff = check_rslt_supp - rslt.supp;
    REQUIRE(diff.norm() <= 1e-4);
    // weights check
    VectorXd check_weights(9);
    check_weights(0) = 0.145777;
    check_weights(1) = 0.0801752;
    check_weights(2) = 0.145777;
    check_weights(3) = 0.0801752;
    check_weights(4) = 0.0961897;
    check_weights(5) = 0.0801752;
    check_weights(6) = 0.145777;
    check_weights(7) = 0.0801752;
    check_weights(8) = 0.145777;
    VectorXd diff_w = check_weights - rslt.weights;
    REQUIRE(diff_w.norm() <= 1e-4);
  }
}
