# Compiling of the program 'Optimal Design Feature Selection'
# Prepare subfolders
set -e
mkdir -p results
mkdir -p build
# Build
cd build
mkdir -p logs
cmake ..
cmake --build . -- -j 10
ctest --verbose
# Copy executable to working directory
cp odfs ../.
