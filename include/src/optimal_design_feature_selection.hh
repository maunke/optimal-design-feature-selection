// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef OPTIMAL_DESIGN_FEATURE_SELECTION_HH
#define OPTIMAL_DESIGN_FEATURE_SELECTION_HH
// -- OPTIMAL DESIGN FEATURE SELECTION -----------------------------------------
// The class 'OptimalDesignFeatureSelection' provides an environment for
// simulating the method of optimal design feature selection.
#include <Eigen/Core>
#include <adaptive_grid_optimal_design.hh>
#include <autodiff.hh>
#include <design_measure.hh>
#include <export_table.hh>
#include <interior_point_method.hh>
#include <iostream>
#include <linear_model.hh>
#include <linear_regression.hh>
#include <logging.hh>
#include <stdexcept>
#include <string>
using namespace Eigen;
// One measurment at a certain step of the method needs the information of the
// previous measured support points.
struct ODFS_Measurements {
  OptimalDesign optimal_design;
  struct Data {
    MatrixXd x;
    VectorXd y;
  } data;
  // Containing all support points of previous measurements
  MatrixXd total_supp;
  // upper bound of Optimal Design weight / meas_perc_step = # measurements at
  // point x related to weight
  double meas_perc_step = 0.04;
};
// Options for simulation
struct OptimalDesignFeatureSelectionOption {
  std::string simulation_id;
  std::string folder_id;
  // x border
  struct Bounds {
    struct X {
      VectorXd l;
      VectorXd u;
    } x;
  } bounds;
  // Grid sampling q
  struct Grid {
    VectorXd sampling;
  } grid;
  // Proof of dependency, take only  1e6 random points before returning result
  int max_proof_dependency_iter = 1e6;
  // Target weight defines the weights of C-,D-,xi-optimality
  struct TargetWeights {
    std::vector<VectorXd> sequence;
    std::string type = "linear";
    double det_weight_min = 0.25;
    double det_weight_max = 0.6;
    double weight_measure_cost = 1.0;
  } target_weights;
  // Termination option of method
  struct Termination {
    int cardinality = 5;
    int supp_number = 20;
  } termination;
  struct Cost {
    std::string type = "default";
    double delta = 2e1;
  } cost;
  struct CCritHorizon {
    int m = 2;
  } ccrit_horizon;
  // All measurements of method
  ODFS_Measurements measurements;
  // Cardinality of nullmodel
  int n0;
  // Oracle with linear model and sigma error
  struct Oracle {
    LinearModel linear_model;
    double sigma;
  } oracle;
  // Export option, #(x,y) tuples of oracle and linear models in every step
  struct ExportOption {
    int sampling_per_dim = 1e3;
  } export_option;
};
// Classical SFFS Methode parameter
struct SFFSParameter {
  std::string selection_type;
  std::vector<LinearModel> pot_linear_models;
  ODFS_Measurements measurements;
  LinearModel linear_model;
  FeatureSet feature_set;
};
// Save results of every step in ODFS method
struct ODFS_Result {
  ODFS_Measurements measurements;
  LinearModel linear_model;
  FeatureSet feature_set;
  std::vector<SFFSParameter> sffs_trace;
};

class OptimalDesignFeatureSelection {
public:
  OptimalDesignFeatureSelection(
      OptimalDesignFeatureSelectionOption option_init) {
    logging::info("Optimal design feature selection initialized.");
    option = option_init;
    parse_option();
  };

  void simulate(LinearModel linear_model, FeatureSet feature_set);
  void export_result();

private:
  ODFS_Result result;
  std::vector<LinearModel> cardinality_linear_models;
  OptimalDesignFeatureSelectionOption option;
  void parse_option();
  void build_target_weights_sequence();
  bool proof_dimension_dependency(LinearModel linear_model);
  void forward_selection(LinearModel linear_model, FeatureSet feature_set);
  void backward_selection(LinearModel linear_model, FeatureSet feature_set);

  int binom_coeff(int n, int k);
  int binom_coeff_to_pos(VectorXi idx, int n, int k);
  void all_subsets_certain_length(int n, int k, MatrixXi &subsets,
                                  VectorXi subset, int i);
  std::vector<LinearModel> horizon_models(LinearModel linear_model,
                                          FeatureSet feature_set);
  double oracle_measurement(VectorXd x);
  void realize_optimal_design(OptimalDesign optimal_design);
  int argmin_rmse_linear_model_id(std::vector<LinearModel> &linear_models);
  double expected_rmse(MatrixXd supp, LinearModel linear_model);
  bool compare_vec(std::vector<int> a, std::vector<int> b);
  void optimal_design_to_total_supp(MatrixXd supp);
};

#endif // OPTIMAL_DESIGN_FEATURE_SELECTION_HH
