// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef ADAPTIVE_GRID_OPTIMAL_DESIGN_HH
#define ADAPTIVE_GRID_OPTIMAL_DESIGN_HH
// -- ADAPTIVE GRID OPTIMAL DESIGN ---------------------------------------------
// Method by
// * Duarte, Belmiro P., Weng Kee Wong und Holger Dette (2018).  Adaptive Grid
// * Semidefinite Programming for Finding Optimal Designs. In: Statistics and
// * Computing 28.2, S. 441–460. issn: 0960-3174.
// * doi: 10.1007/s11222-017-9741-y.
// * url: https://doi.org/10.1007/ s11222-017-9741-y
#include <Eigen/Core>
#include <autodiff.hh>
#include <design_measure.hh>
#include <export_table.hh>
#include <interior_point_method.hh>
#include <iostream>
#include <linear_model.hh>
#include <logging.hh>
#include <stdexcept>
#include <string>
using namespace Eigen;

struct OptimalDesign {
  MatrixXd supp;
  VectorXd weights;
  double measure;
};

struct AdapativeGridOptimalDesignOption {
  std::string simulation_id;
  std::string folder_id;
  // adaptive grid optimization parameters
  double filter_epsilon = 1e-4;
  double collapse_epsilon = 1e-4;
  double supp_precision = 1e-5;
  double measure_precision = 1e-6;
  int max_iter = 1e2;
  struct Weights {
    NLPMinimizeOption minimize;
    NLPSolverOption solver;
  } weights;
  struct Supp {
    NLPMinimizeOption minimize;
    NLPSolverOption solver;
  } supp;
  // trace Adaptive Grid Optimal Design Method
  struct Trace {
    bool dispersion = false;
    int dispersion_sampling_per_dim = 1e3;
    bool weights = false;
    bool measure = false;
    std::vector<std::string> measures;
  } trace;
};

struct DesignOption {
  std::string design_type;
  std::string cost_type = "default";
  double cost_delta;
  VectorXd measured_supp_ids;
  bool measured_supp_set = false;
  VectorXd target_weights;
  int c_crit_m;
  std::vector<LinearModel> linear_models;
};

class AdapativeGridOptimalDesign {
public:
  AdapativeGridOptimalDesign(DesignOption design_option_init,
                             AdapativeGridOptimalDesignOption option_init) {
    logging::info("Adaptive grid optimal design initialized.");
    design_option = design_option_init;
    option = option_init;
    logging_options();
  };
  OptimalDesign solve(MatrixXd supp_grid, bool shuffle_option = true);
  double measure(OptimalDesign &design);

private:
  AdapativeGridOptimalDesignOption option;
  DesignOption design_option;
  // filter weights < epsilon
  void logging_options();
  void update_minimize_weights_option(NLPMinimizeOption &min_opt, MatrixXd supp,
                                      VectorXd x0);
  void filter_design(OptimalDesign &optimal_design);
  void collapse_design(OptimalDesign &optimal_design);

  void trace_measure(OptimalDesign &design);
  void minimize_weights(OptimalDesign &optimal_design);
  void minimize_supp(OptimalDesign &optimal_design);

  void export_weights(OptimalDesign &optimal_design, int iter);
  void export_measure();
  void export_dispersion(OptimalDesign &design, int iter);
};

/** Design Effiencies
 * - D-efficiency = (det design / det optimal design)^(1/n)
 */
class DesignEfficiency {
public:
  DesignEfficiency(MatrixXd suboptimal_supp_init,
                   VectorXd suboptimal_weights_init,
                   LinearModel linear_model_init, VectorXd bounds_x_l_init,
                   VectorXd bounds_x_u_init, VectorXd bounds_x_q_init) {
    suboptimal_supp = suboptimal_supp_init;
    suboptimal_weights = suboptimal_weights_init;
    linear_model = linear_model_init;
    bounds_x_l = bounds_x_l_init;
    bounds_x_u = bounds_x_u_init;
    bounds_x_q = bounds_x_q_init;
  }

  double d_crit() {
    // suboptimal d crit measure = |M|^{-1}
    DesignMeasureDCriteriumWeights d_crit_weights(suboptimal_supp,
                                                  linear_model);
    d_crit_weights.function.set_x(suboptimal_weights);
    // feauures
    double feature_size = (double)linear_model.feature_size();
    double suboptimal_d_crit =
        std::pow(std::exp(-d_crit_weights.function.val()), 1.0 / feature_size);
    // optimal d crit measure
    DesignOption design_option;
    design_option.design_type = "d-criterium";
    design_option.linear_models.push_back(linear_model);
    DesignMeasureGrid grid(bounds_x_l, bounds_x_u, bounds_x_q);
    AdapativeGridOptimalDesignOption adaptive_grid_option;
    adaptive_grid_option.supp.minimize.bounds.x.u = bounds_x_u;
    adaptive_grid_option.supp.minimize.bounds.x.l = bounds_x_l;
    AdapativeGridOptimalDesign adaptive_grid_method(design_option,
                                                    adaptive_grid_option);
    OptimalDesign optimal_design = adaptive_grid_method.solve(grid.supp);
    double optimal_d_crit = optimal_design.measure;
    // d-crit eff
    double d_crit_eff = suboptimal_d_crit / optimal_d_crit;
    return d_crit_eff;
  }

private:
  MatrixXd suboptimal_supp;
  VectorXd suboptimal_weights;
  LinearModel linear_model;
  VectorXd bounds_x_l;
  VectorXd bounds_x_u;
  VectorXd bounds_x_q;
};

#endif // ADAPTIVE_GRID_OPTIMAL_DESIGN_HH
