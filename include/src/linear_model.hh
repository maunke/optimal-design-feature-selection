// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef LINEAR_MODEL_HH
#define LINEAR_MODEL_HH
// -- LINEAR MODEL -------------------------------------------------------------
// Perform actions on a linear model as set value x, get coefficients,
// add/delete a feature,...
// Features are defined as an automatic differentiation feature.
#include <Eigen/Core>
#include <autodiff.hh>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>
using namespace Eigen;

struct LinearModelFeature {
  AD_Decl_Function (*function)(AD_Vector, VectorXd);
  int id;
  VectorXd gen;
};

typedef std::vector<LinearModelFeature> FeatureSet;

// linear model: sum of features
class LinearModel {
public:
  LinearModel(){};
  // set/get x vector
  void set_x(VectorXd arg);
  VectorXd get_x();
  // set/get coefficients
  void set_coeff(VectorXd arg);
  VectorXd get_coeff();
  // add/delete/get feature
  void add_feature(LinearModelFeature new_feature);
  void delete_feature(int feat_id);
  int feature_size();
  LinearModelFeature feature(int feat_id);
  // get feature ids
  std::vector<int> features_id();
  // value of model or features
  double val(int = -1);
  // gradient of model or features
  VectorXd grad(int = -1);
  // hessian of model or features
  MatrixXd hes(int = -1);
  // design vector
  VectorXd design();
  // design derivation, after del x_i and optionally del x_i del x_j
  VectorXd design_deriv(int i, int j = -1);
  // jacobian matrix
  MatrixXd jac();
  // properties
  double rmse = std::numeric_limits<double>::quiet_NaN();

private:
  int features_size = 0;
  // coefficients vector
  VectorXd coeff;
  // x vector
  VectorXd x;
  // array of declared features
  FeatureSet decl_features;
  // features as ad function
  std::vector<AD_Function> features;
  // proof coeff size
  void proof_coeff_size();
  // proof feature size
  void proof_feature_size();
};
#endif // LINEAR_MODEL_HH
