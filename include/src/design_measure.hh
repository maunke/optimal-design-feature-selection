// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef DESIGN_MEASURE_HH
#define DESIGN_MEASURE_HH
// -- DESIGN MEASURE -----------------------------------------------------------
// Measures: C-,D-,Xi-Optimality
#include <Eigen/Core>
#include <autodiff.hh>
#include <interior_point_method.hh>
#include <iostream>
#include <linear_model.hh>
#include <stdexcept>
#include <string>
using namespace Eigen;
// Building initial grid for adaptive grid optimization in dependence of a given
// set of existing support vectors.
class DesignMeasureGrid {
public:
  DesignMeasureGrid(VectorXd x_l_init, VectorXd x_u_init, VectorXd x_q_init,
                    MatrixXd measured_supp_init = MatrixXd::Zero(0, 0)) {
    x_l = x_l_init.array();
    x_u = x_u_init.array();
    x_q = x_q_init.array();
    if (measured_supp_init.cols() > 0) {
      measured_supp_set = true;
      measured_supp = measured_supp_init;
      measured_supp_ids = VectorXd::Constant(measured_supp.cols(), -1);
    }
    int dim = x_u.size();
    int supp_size = x_q.prod();
    supp = MatrixXd::Zero(dim, supp_size);
    ArrayXd pos = ArrayXd::Zero(dim);
    ArrayXd x(dim);
    build_supp(pos, 0, x);
    // size of missing measured points in supp
    if (measured_supp_set) {
      int miss_meas_size = 0;
      for (int i = 0; i < measured_supp_ids.size(); ++i) {
        if (-1 == measured_supp_ids(i)) {
          miss_meas_size++;
        }
      }
      // add remaining measured supp points
      MatrixXd supp_tmp(dim, supp_size + miss_meas_size);
      supp_tmp.block(0, 0, dim, supp_size) = supp;
      int add_pos = 0;
      for (int i = 0; i < measured_supp_ids.size(); ++i) {
        if (-1 == measured_supp_ids(i)) {
          measured_supp_ids(i) = supp_size + add_pos;
          supp_tmp.col(supp_size + add_pos) = measured_supp.col(i);
          add_pos++;
        }
      }
      supp = supp_tmp;
    }
  };

  MatrixXd supp;
  VectorXd measured_supp_ids;

private:
  ArrayXd x_u;
  ArrayXd x_l;
  ArrayXd x_q;
  bool measured_supp_set = false;
  MatrixXd measured_supp;
  void build_supp(ArrayXd pos, int d, ArrayXd x);
};

class DesignMeasureHelper {
public:
  DesignMeasureHelper(){};
  // build normalized fim
  MatrixXd norm_fim(VectorXd &weights, MatrixXd &design);
  // build design matrix
  MatrixXd design_matrix(MatrixXd &supp_points, LinearModel &linear_model);
};

// Determinant Criterium Weights
FunctionTargetResult
determinant_criterium_weights(VectorXd &weights, VectorXd &vec_arg,
                              MatrixXd &design,
                              std::vector<LinearModel> &linear_models);

class DesignMeasureDCriteriumWeights {
public:
  DesignMeasureDCriteriumWeights(MatrixXd &supp_points,
                                 LinearModel &linear_model) {
    // build design matrix
    MatrixXd design = helper.design_matrix(supp_points, linear_model);
    // set target
    function_target.coeff_mat = design;
    function_target.compute = determinant_criterium_weights;
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;

private:
  // helper
  DesignMeasureHelper helper;
};
// Determinant Criterium Supp
FunctionTargetResult
determinant_criterium_supp(VectorXd &x, VectorXd &vec_arg, MatrixXd &fim,
                           std::vector<LinearModel> &linear_models);

class DesignMeasureDCriteriumSupp {
public:
  DesignMeasureDCriteriumSupp(VectorXd &weights, MatrixXd &supp_points,
                              LinearModel &linear_model) {
    MatrixXd design = helper.design_matrix(supp_points, linear_model);
    MatrixXd fim = helper.norm_fim(weights, design);
    // set target
    function_target.coeff_mat = fim;
    function_target.linear_models.push_back(linear_model);
    function_target.compute = determinant_criterium_supp;
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;

private:
  // helper
  DesignMeasureHelper helper;
};

// C-Criterium Weights
FunctionTargetResult
c_criterium_weights(VectorXd &weights, VectorXd &vec_arg, MatrixXd &design,
                    std::vector<LinearModel> &linear_models);

class DesignMeasureCCriteriumWeights {
public:
  DesignMeasureCCriteriumWeights(int m, MatrixXd &supp_points,
                                 LinearModel &linear_model) {
    // build design matrix
    MatrixXd design = helper.design_matrix(supp_points, linear_model);
    // set target
    function_target.coeff_vec = VectorXd::Constant(1, m);
    function_target.coeff_mat = design;
    function_target.compute = c_criterium_weights;
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;

private:
  // helper
  DesignMeasureHelper helper;
};

// C-Criterium Supp
FunctionTargetResult c_criterium_supp(VectorXd &x, VectorXd &vec_arg,
                                      MatrixXd &fim,
                                      std::vector<LinearModel> &linear_models);

class DesignMeasureCCriteriumSupp {
public:
  DesignMeasureCCriteriumSupp(int m, VectorXd &weights, MatrixXd &supp_points,
                              LinearModel &linear_model) {
    MatrixXd design = helper.design_matrix(supp_points, linear_model);
    MatrixXd fim = helper.norm_fim(weights, design);
    // set target
    function_target.coeff_vec = VectorXd::Constant(1, m);
    function_target.coeff_mat = fim;
    function_target.linear_models.push_back(linear_model);
    function_target.compute = c_criterium_supp;
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;

private:
  // helper
  DesignMeasureHelper helper;
};

// Measurements Cost-Function
// default cost: log(1+delta*w)
FunctionTargetResult
cost_weight_default(VectorXd &weights, VectorXd &vec_arg, MatrixXd &vec_mat,
                    std::vector<LinearModel> &linear_models);

class DesignMeasureCostWeights {
public:
  DesignMeasureCostWeights(std::string cost_type, VectorXd &arg) {
    if ("default" == cost_type) {
      // arg: (measured_supp_ids,delta-coeff)
      function_target.coeff_vec = arg;
      function_target.compute = cost_weight_default;
    }
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;
};

FunctionTargetResult cost_supp_default(VectorXd &x, VectorXd &vec_arg,
                                       MatrixXd &vec_mat,
                                       std::vector<LinearModel> &linear_models);
class DesignMeasureCostSupp {
public:
  DesignMeasureCostSupp(std::string cost_type, int x_size) {
    if ("default" == cost_type) {
      function_target.coeff_vec = VectorXd::Zero(x_size);
      function_target.coeff_mat = MatrixXd::Zero(x_size, x_size);
      function_target.compute = cost_supp_default;
    }
    function.add_target(function_target);
  };
  // function
  NLPFunction function;
  // function target
  FunctionTarget function_target;
};

// ODFS
class DesignMeasureODFSWeights {
public:
  DesignMeasureODFSWeights(std::vector<LinearModel> &linear_models,
                           MatrixXd &supp_points, int m,
                           VectorXd &target_weights, std::string cost_type,
                           VectorXd &measured_supp_ids, double cost_delta) {
    // d-criterium
    DesignMeasureDCriteriumWeights d_crit(supp_points, linear_models[0]);
    function.add_target(d_crit.function_target, target_weights(0));
    // c-criterium
    for (int i = 1; i < linear_models.size(); ++i) {
      DesignMeasureCCriteriumWeights c_crit(m, supp_points, linear_models[i]);
      function.add_target(c_crit.function_target,
                          target_weights(1) /
                              ((double)linear_models.size() - 1));
    }
    // cost-criterium
    if (measured_supp_ids.size() > 0) {
      VectorXd cost_arg(measured_supp_ids.size() + 1);
      cost_arg.head(measured_supp_ids.size()) = measured_supp_ids;
      cost_arg(measured_supp_ids.size()) = cost_delta;
      DesignMeasureCostWeights cost_crit(cost_type, cost_arg);
      function.add_target(cost_crit.function_target, target_weights(2));
    }
  };
  // function
  NLPFunction function;
};

class DesignMeasureODFSSupp {
public:
  DesignMeasureODFSSupp(std::vector<LinearModel> &linear_models,
                        VectorXd &weights, MatrixXd &supp_points, int m,
                        VectorXd &target_weights, std::string cost_type,
                        VectorXd &measured_supp_ids, double cost_delta) {
    // d-criterium
    DesignMeasureDCriteriumSupp d_crit(weights, supp_points, linear_models[0]);
    function.add_target(d_crit.function_target, target_weights(0));
    // c-criterium
    for (int i = 1; i < linear_models.size(); ++i) {
      DesignMeasureCCriteriumSupp c_crit(m, weights, supp_points,
                                         linear_models[i]);
      function.add_target(c_crit.function_target,
                          target_weights(1) /
                              ((double)linear_models.size() - 1));
    }
    // cost-criterium
    // if (measured_supp_ids.size() > 0) {
    //   int x_size = supp_points.rows();
    //   DesignMeasureCostSupp cost_crit(cost_type, x_size);
    //   function.add_target(cost_crit.function_target, target_weights(2));
    // }
  };
  // function
  NLPFunction function;
};

#endif // DESIGN_MEASURE_HH
