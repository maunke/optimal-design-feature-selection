// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#ifndef INTERIOR_POINT_METHOD_HH
#define INTERIOR_POINT_METHOD_HH
// -- NON LINEAR PROBLEM SOLVER ------------------------------------------------
// Find a minimum of a problem under (in-)equality restrictions.
#include <Eigen/Core>
#include <autodiff.hh>
#include <iostream>
#include <linear_model.hh>
#include <logging.hh>
#include <stdexcept>
#include <string>
using namespace Eigen;
// A function gives a value, gradient or hessian
struct FunctionTargetResult {
  double value;
  VectorXd gradient;
  MatrixXd hessian;
};
// Function target with constant coeffcients, matrices or linear models for
// computing the FunctionTargetResult
struct FunctionTarget {
  FunctionTargetResult (*compute)(VectorXd &, VectorXd &, MatrixXd &,
                                  std::vector<LinearModel> &);
  VectorXd coeff_vec;
  MatrixXd coeff_mat;
  std::vector<LinearModel> linear_models;
};

// NLP function
class NLPFunction {
public:
  NLPFunction(){};
  void add_target(FunctionTarget nlp_target_init, double coeff = 1.0) {
    nlp_targets.push_back(nlp_target_init);
    nlp_target_coeffs.push_back(coeff);
  }
  void set_x(VectorXd x) {
    x_size = x.size();
    nlp_target_results.clear();
    for (int i = 0; i < nlp_targets.size(); ++i) {
      FunctionTargetResult rslt = nlp_targets[i].compute(
          x, nlp_targets[i].coeff_vec, nlp_targets[i].coeff_mat,
          nlp_targets[i].linear_models);
      nlp_target_results.push_back(rslt);
    }
  }
  double val() {
    double value = 0.0;
    for (int i = 0; i < nlp_target_results.size(); ++i) {
      value += nlp_target_coeffs[i] * nlp_target_results[i].value;
      // std::cout << "i: " << i << ", value: " << value << '\n';
    }
    return value;
  }
  VectorXd grad() {
    VectorXd gradient = VectorXd::Zero(x_size);
    for (int i = 0; i < nlp_target_results.size(); ++i) {
      gradient += nlp_target_coeffs[i] * nlp_target_results[i].gradient;
    }
    return gradient;
  }
  MatrixXd hes() {
    MatrixXd hessian = MatrixXd::Zero(x_size, x_size);
    for (int i = 0; i < nlp_target_results.size(); ++i) {
      hessian += nlp_target_coeffs[i] * nlp_target_results[i].hessian;
    }
    return hessian;
  }

private:
  int x_size;
  std::vector<double> nlp_target_coeffs;
  std::vector<FunctionTarget> nlp_targets;
  std::vector<FunctionTargetResult> nlp_target_results;
};

// NLP Options
struct NLPSolverOption {
  // precision
  struct Prec {
    double barrier = 1e-8;
    double newton = 1e-6;
  } prec;
  // maximal iteration
  struct MaxIter {
    int barrier = 1e3;
    int backline = 1e1;
    int newton = 1e3;
  } max_iter;
  // barrier
  struct Barrier {
    double mu = 5;
    double t0 = 100;
  } barrier;
  // backline
  struct Backline {
    double lambda = 0.2;
    double beta = 0.1;
  } backline;
};
// NLP Minimize Option
struct NLPMinimizeOption {
  // init x0
  VectorXd x0;
  // x border
  struct Bounds {
    struct X {
      VectorXd l;
      VectorXd u;
    } x;
  } bounds;
  /* linear equality constraint: A*x = b for all x in domain
   * - mat: A
   */
  struct LinearEqualityConstraint {
    MatrixXd mat = MatrixXd::Zero(0, 0);
    MatrixXd newton_constr_mat;
    bool enabled = false;
  } lin_equal_constr;
  /* inequality constraints: f(x) <= 0
   * - mat: A
   */
  struct InequalityConstraint {
    // set template: custom
    std::string use_template = "none";
    int voronoi_x_id;
    MatrixXd voronoi_supp;
    NLPFunction function;
    FunctionTargetResult result;
    bool enabled = false;
  } inequal_constr;
};

// Precompute efficiently the elements of voronoi restriction
FunctionTargetResult
nlp_solver_helper_voronoi_target(VectorXd &x, VectorXd &x_id_vec,
                                 MatrixXd &supp,
                                 std::vector<LinearModel> &linear_models);
class NLPSolverHelperVoronoi {
public:
  NLPSolverHelperVoronoi(int &x_id, MatrixXd &supp) {
    function_target.coeff_mat.resize(supp.rows(), 2 * supp.cols());
    function_target.coeff_mat.block(0, 0, supp.rows(), supp.cols()) = supp;
    // pre compute diffs squared norms
    function_target.coeff_vec.resize(supp.cols() + 1);
    for (int i = 0; i < supp.cols(); ++i) {
      if (i != x_id) {
        function_target.coeff_vec(i) =
            supp.col(x_id).squaredNorm() - supp.col(i).squaredNorm();
        function_target.coeff_mat.col(supp.cols() + i) =
            2 * (supp.col(x_id) - supp.col(i));
      }
    }
    function_target.coeff_vec(supp.cols()) = x_id;
    function_target.compute = nlp_solver_helper_voronoi_target;
    function.add_target(function_target);
  };

  NLPFunction function;

private:
  FunctionTarget function_target;
};

// NLP Solver
class NLPSolver {
public:
  NLPSolver(NLPFunction function_init, NLPSolverOption option_init) {
    logging::info("NLPSolver initialized.");
    function = function_init;
    option = option_init;
  };

  VectorXd minimize(NLPMinimizeOption minimize_option_init);

private:
  NLPFunction function;
  NLPSolverOption option;
  // parse minimize option
  NLPMinimizeOption minimize_option;
  void parse_minimize_option(NLPMinimizeOption minimize_option_init);
  // algorithm functions
  VectorXd barrier_method();
  VectorXd newton_method(VectorXd &x, double t);
  void backline_search(VectorXd &x, VectorXd &delta_x, bool &backline_exceeded);
  // set x: decl_function, inequal_constr.decl_function
  void set_x(VectorXd &x);
  // helper functions
  void pre_build_newton_linear_constraint_matrix();
  void compute_log_barrier_function(VectorXd &x);
  bool feasibility_check(VectorXd &x);
};

#endif // INTERIOR_POINT_METHOD_HH
