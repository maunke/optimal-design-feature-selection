// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <Eigen/Dense>
#include <limits>
#include <linear_regression.hh>
using namespace Eigen;
// -- Linear Regression --------------------------------------------------------
// Fit = Find coefficients and get rmse related to trained data set
void LinearRegression::fit(LinearModel &linear_model) {
  // fit linear model
  int feat_size = linear_model.feature_size();
  MatrixXd design_matrix(meas_size, feat_size);
  for (int i = 0; i < meas_size; ++i) {
    linear_model.set_x(measurements.x.col(i));
    design_matrix.row(i) = linear_model.design();
  }

  VectorXd coeff = (design_matrix.transpose() * design_matrix)
                       .lu()
                       .solve(design_matrix.transpose() * measurements.y);
  linear_model.set_coeff(coeff);
  // calculate rmse
  linear_model.rmse = std::sqrt(1.0 / (double)meas_size) *
                      (design_matrix * coeff - measurements.y).norm();
}
