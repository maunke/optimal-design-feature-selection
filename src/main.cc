// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <logging.hh>
#include <simulation.hh>
// -- MAIN ---------------------------------------------------------------------
// Run free configurable simulations (see simulation.cc/.hh), such as finding an
// optimal design for a certain (quasi-)linear model or a scenario for
// researching in the method of the optimal design feature selection.
int main(int argc, char **argv) {
  // -- START SIMULATION -------------------------------------------------------
  // Optimal Design
  // Sampling Points
  // int q = 1001;
  // // // Detailed Trace Information
  // bool detailed = true;
  // // Run Simulation
  // Simulation::Dette_Model_1(q, detailed);

  SimulationOption simulation_option;
  simulation_option.oracle.sigma = 0.01;
  simulation_option.m = 3;
  Simulation::ODFS_Polynomial(simulation_option);

  return 0;
}
