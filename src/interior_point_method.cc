// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <Eigen/Dense>
#include <interior_point_method.hh>
#include <limits>
using namespace Eigen;
// -- NON LINEAR PROBLEM SOLVER ------------------------------------------------
// Find a minimum of a problem under (in-)equality restrictions.
// Minimize via barrier method
VectorXd NLPSolver::minimize(NLPMinimizeOption minimize_option_init) {
  logging::info("NLPSolver | Start minimizing.");
  // check minimize options
  parse_minimize_option(minimize_option_init);
  // find optimal x via barrier method
  VectorXd x_opt = barrier_method();
  return x_opt;
}
// parse minimize option
void NLPSolver::parse_minimize_option(NLPMinimizeOption minimize_option_init) {
  minimize_option = minimize_option_init;
  // check linear equality constraint
  if (minimize_option.lin_equal_constr.mat.rows() > 0 &&
      minimize_option.lin_equal_constr.mat.cols() ==
          minimize_option.x0.size()) {
    minimize_option.lin_equal_constr.enabled = true;
    // pre build newton linear constraint matrix
    pre_build_newton_linear_constraint_matrix();
  }
  // check inequality constraint
  if (minimize_option.inequal_constr.use_template != "none" ||
      (minimize_option.bounds.x.l.size() == minimize_option.x0.size() &&
       minimize_option.bounds.x.l.size() ==
           minimize_option.bounds.x.u.size())) {
    minimize_option.inequal_constr.enabled = true;
  }
}
// set x of decl.function and inequality functions
void NLPSolver::set_x(VectorXd &x) {
  // NLP function
  function.set_x(x);
  // barrier function from inequality constraint
  if (minimize_option.inequal_constr.enabled) {
    compute_log_barrier_function(x);
  }
}
// build newton linear constraint matrix
void NLPSolver::pre_build_newton_linear_constraint_matrix() {
  int x_size = minimize_option.x0.size();
  int mat_rows = minimize_option.lin_equal_constr.mat.rows();
  int newton_mat_size = x_size + mat_rows;
  MatrixXd newton_constr_mat = MatrixXd::Zero(newton_mat_size, newton_mat_size);
  newton_constr_mat.block(x_size, 0, mat_rows, x_size) =
      minimize_option.lin_equal_constr.mat;
  newton_constr_mat.block(0, x_size, x_size, mat_rows) =
      minimize_option.lin_equal_constr.mat.transpose();
  minimize_option.lin_equal_constr.newton_constr_mat = newton_constr_mat;
}
// build log barrier function
void NLPSolver::compute_log_barrier_function(VectorXd &x) {
  int x_size = x.size();
  double value = 0;
  VectorXd gradient = VectorXd::Zero(x_size);
  MatrixXd hessian = MatrixXd::Zero(x_size, x_size);
  // check for bounds
  ArrayXd x_l = minimize_option.bounds.x.l.array();
  ArrayXd x_u = minimize_option.bounds.x.u.array();
  ArrayXd x_array = x.array();
  if (x_l.size() == x_u.size() && x_l.size() == x_size) {
    // value
    for (int i = 0; i < x_size; ++i) {
      value -= std::log(x_u(i) - x_array(i));
      value -= std::log(x_array(i) - x_l(i));
    }
    // gradient
    gradient = 1 / (x_u - x_array) + 1 / (x_l - x_array);
    // hessian
    VectorXd hessian_diag(x_size);
    hessian_diag = 1 / pow(x_u - x_array, 2) + 1 / pow(x_l - x_array, 2);
    hessian += hessian_diag.asDiagonal();
  }
  // custom template
  if ("custom" == minimize_option.inequal_constr.use_template) {
    minimize_option.inequal_constr.function.set_x(x);

    double func_val;
    VectorXd func_grad;
    MatrixXd func_hes;
    func_val = minimize_option.inequal_constr.function.val();
    func_grad = minimize_option.inequal_constr.function.grad();
    func_hes = minimize_option.inequal_constr.function.hes();
    value -= std::log(-func_val);
    gradient -= 1 / func_val * func_grad;
    hessian += 1 / std::pow(func_val, 2) * func_grad * func_grad.transpose() -
               1 / func_val * func_hes;
  } else if ("voronoi" == minimize_option.inequal_constr.use_template) {
    NLPSolverHelperVoronoi voronoi_helper(
        minimize_option.inequal_constr.voronoi_x_id,
        minimize_option.inequal_constr.voronoi_supp);
    NLPFunction voronoi_function = voronoi_helper.function;
    voronoi_function.set_x(x);
    value += voronoi_function.val();
    gradient += voronoi_function.grad();
    hessian += voronoi_function.hes();
  }
  // set value, gradient, hessian
  minimize_option.inequal_constr.result.value = value;
  minimize_option.inequal_constr.result.gradient = gradient;
  minimize_option.inequal_constr.result.hessian = hessian;
}
// barrier method
VectorXd NLPSolver::barrier_method() {
  logging::info("NLPSolver | Start barrier method.");
  VectorXd x = minimize_option.x0;
  // start value
  int x_size = x.size();
  option.barrier.t0 =
      std::max(2e2, std::min(option.barrier.t0 * std::sqrt(x_size), 5e3));
  double t = option.barrier.t0;
  int iter = 0;
  while (iter++ < option.max_iter.barrier &&
         x_size / t >= option.prec.barrier) {
    x = newton_method(x, t);
    t *= option.barrier.mu;
  }
  logging::info("NLPSolver | Barrier method finished after " +
                std::to_string(iter - 1) + " iterations");
  return x;
}
// newton method
VectorXd NLPSolver::newton_method(VectorXd &x, double t) {
  logging::info("NLPSolver | Start newton method.");
  int x_size = x.size();
  double iter_barrier = t / option.barrier.t0 / option.barrier.mu;
  // initialize newton equation parameters
  MatrixXd A;
  VectorXd b;
  VectorXd delta_x;
  // set A, b, delta_x size
  if (minimize_option.lin_equal_constr.enabled) {
    A = minimize_option.lin_equal_constr.newton_constr_mat;
  } else {
    A = MatrixXd::Zero(x_size, x_size);
  }
  b = VectorXd::Zero(A.rows());
  delta_x = VectorXd::Zero(A.rows());
  int iter = 0;
  double crit = std::numeric_limits<double>::max();
  bool backline_exceeded = false;
  function.set_x(x);
  // Newton precision is modified/boosted in relation to the iteration number of
  // the barrier method. Idea: Newton method has to be accurate if the barrier
  // method pushed the local problem to the final minimum region.
  while (iter++ < option.max_iter.newton &&
         crit >= option.prec.newton *
                     std::max(1.0, 1e4 * std::pow(2, -iter_barrier)) &&
         !backline_exceeded) {
    // condition inequality constraints
    if (minimize_option.lin_equal_constr.enabled) {
      A.block(0, 0, x_size, x_size) = t * function.hes();
      b.head(x_size) = -t * function.grad();
      if (minimize_option.inequal_constr.enabled) {
        compute_log_barrier_function(x);
        A.block(0, 0, x_size, x_size) +=
            minimize_option.inequal_constr.result.hessian;
        b.head(x_size) -= minimize_option.inequal_constr.result.gradient;
      }
      delta_x = A.lu().solve(b).head(x_size);
    } else {
      A = t * function.hes();
      b = -t * function.grad();
      if (minimize_option.inequal_constr.enabled) {
        compute_log_barrier_function(x);
        A += minimize_option.inequal_constr.result.hessian;
        b -= minimize_option.inequal_constr.result.gradient;
      }
      delta_x = A.lu().solve(b);
    }
    // criterion
    crit = delta_x.norm();
    // backline search
    backline_search(x, delta_x, backline_exceeded);
  }
  logging::info("NLPSolver | Newton method finished after " +
                std::to_string(iter - 1) + " iterations");
  return x;
}

void NLPSolver::backline_search(VectorXd &x, VectorXd &delta_x,
                                bool &backline_exceeded) {
  // normalize delta_x if norm > 1
  double delta_x_norm = delta_x.norm();
  if (delta_x_norm > 1) {
    delta_x /= delta_x_norm;
  }
  double lambda = option.backline.lambda;
  int iter = 0;
  bool search = true;
  VectorXd x_tmp;
  double old_val = function.val();
  while (iter++ < option.max_iter.backline && search) {
    x_tmp = x + lambda * delta_x;
    function.set_x(x_tmp);
    if (function.val() < old_val && feasibility_check(x_tmp)) {
      search = false;
      x = x_tmp;
    }
    lambda *= option.backline.beta;
  }
  if (iter >= option.max_iter.backline) {
    backline_exceeded = true;
  }
}

bool NLPSolver::feasibility_check(VectorXd &x) {
  bool check = true;
  if ((x - minimize_option.bounds.x.l).minCoeff() < 0 ||
      (x - minimize_option.bounds.x.u).maxCoeff() > 0) {
    check = false;
  }
  return check;
}
// Compute efficiently the value, gradient, hessian of voronoi constraints
FunctionTargetResult
nlp_solver_helper_voronoi_target(VectorXd &x, VectorXd &pre_compute_and_id,
                                 MatrixXd &supp_and_diff,
                                 std::vector<LinearModel> &linear_models) {
  int supp_size = supp_and_diff.cols() / 2;
  int x_size = x.size();
  MatrixXd supp = supp_and_diff.block(0, 0, supp_and_diff.rows(), supp_size);
  MatrixXd diff =
      supp_and_diff.block(0, supp_size, supp_and_diff.rows(), supp_size);
  VectorXd squared_norm_diff = pre_compute_and_id.head(supp_size);
  int x_id = pre_compute_and_id(supp_size);
  VectorXd tmp_f(supp_size);
  for (int i = 0; i < supp_size; ++i) {
    if (i != x_id) {
      tmp_f(i) = squared_norm_diff(i) - x.transpose() * diff.col(i);
    }
  }
  FunctionTargetResult rslt;
  rslt.value = 0.0;
  rslt.gradient = VectorXd::Zero(x_size);
  rslt.hessian = MatrixXd::Zero(x_size, x_size);
  VectorXd tmp(x_size);
  for (int i = 0; i < supp_size; ++i) {
    if (i != x_id) {
      tmp = diff.col(i) / tmp_f(i);
      rslt.value -= std::log(-tmp_f(i));
      rslt.gradient += tmp;
      rslt.hessian += tmp * tmp.transpose();
    }
  }
  return rslt;
}
