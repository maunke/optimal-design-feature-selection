// This file is part of the program 'Optimal Design Feature Selection'.
//
// Copyright (C) 2019  Markus Unkel <markus@unkel.io>
//
// 'Optimal Design Feature Selection' is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
#include <Eigen/Dense>
#include <ctime>
#include <limits>
#include <optimal_design_feature_selection.hh>
#include <random>
using namespace Eigen;
// -- OPTIMAL DESIGN FEATURE SELECTION -----------------------------------------
// The class 'OptimalDesignFeatureSelection' provides an environment for
// simulating the method of optimal design feature selection.
// See informations of this and related classes in the header file
// 'optimal_design_feature_selection.hh'.

// Prebuild the weights of method measure for every cardinality step
void OptimalDesignFeatureSelection::build_target_weights_sequence() {
  double alpha;
  VectorXd target_weights(3);
  int card = option.termination.cardinality;
  if ("linear" == option.target_weights.type) {
    // cost function weight
    target_weights(2) = option.target_weights.weight_measure_cost;
    double weight_cost_rest = 1 - option.target_weights.weight_measure_cost;
    // build linear alpha sequence
    alpha = option.target_weights.det_weight_min;
    double step =
        (option.target_weights.det_weight_max - alpha) / ((double)card - 2);
    for (int i = 0; i < card - 1; ++i) {
      target_weights(0) = weight_cost_rest * alpha;
      target_weights(1) = weight_cost_rest * (1.0 - alpha);
      option.target_weights.sequence.push_back(target_weights);
      alpha += step;
    }
  }
}
// Parse and check the input options before computing the simulation.
void OptimalDesignFeatureSelection::parse_option() {
  // proof size of c-crit-m
  if (option.ccrit_horizon.m < 2) {
    throw std::invalid_argument("odfs: c-crit-m is less than 2");
  }
  logging::info("ODFS:C-Crit | horizon m = " +
                std::to_string(option.ccrit_horizon.m));
  // check bounds
  if (option.termination.cardinality < 1 ||
      option.termination.supp_number < 1) {
    std::string info;
    info +=
        "cardinality: " + std::to_string(option.termination.cardinality) + ", ";
    info += "support number: " + std::to_string(option.termination.supp_number);
    throw std::invalid_argument(
        "odfs: cardinality/rmse/supp-number non feasible. " + info);
  }
  logging::info("ODFS:Option:Termination | Cardinality = " +
                std::to_string(option.termination.cardinality));
  logging::info("ODFS:Option:Termination | #SupportVectors = " +
                std::to_string(option.termination.supp_number));
  // check target weights sequence
  if (option.target_weights.sequence.size() == 0) {
    if (option.target_weights.type == "custom") {
      throw std::invalid_argument(
          "odfs: custom target weights given, but size of sequence = 0");
    } else {
      build_target_weights_sequence();
    }
  } else if (option.target_weights.sequence.size() <
             option.termination.cardinality) {
    throw std::invalid_argument(
        "odfs: target weights sequence size < cardinality. " +
        std::to_string(option.target_weights.sequence.size()) + " + " +
        std::to_string(option.termination.cardinality));
  }
  // proof size of c-crit-m
  if (option.target_weights.det_weight_min < 0.0) {
    throw std::invalid_argument(
        "odfs: target det weight min " +
        std::to_string(option.target_weights.det_weight_min) + " <= 0.0 !");
  }
  if (option.target_weights.det_weight_max > 1.0) {
    throw std::invalid_argument(
        "odfs: target det weight min " +
        std::to_string(option.target_weights.det_weight_max) + " >= 1.0 !");
  }
  logging::info("ODFS:Option:Target-Weights | Type: " +
                option.target_weights.type);
  if (option.target_weights.type != "custom") {
    logging::info("ODFS:Option:Target-Weights | det weight min = " +
                  std::to_string(option.target_weights.det_weight_min) +
                  ", det weight max = " +
                  std::to_string(option.target_weights.det_weight_max));
  }
  // create empty measured data
  int x_size = option.bounds.x.l.size();
  if (option.measurements.data.x.cols() < 1) {
    option.measurements.data.x = MatrixXd(x_size, 0);
    option.measurements.data.y = VectorXd(0);
    // create empty supp
    option.measurements.optimal_design.supp = MatrixXd(x_size, 0);
  }
  // total supp set to 0
  option.measurements.total_supp = MatrixXd(x_size, 0);
  logging::info("ODFS:Option | Parse options done.");
}
// Initially and in the part of the backward selection we have to proof the
// dimension dependency of potential models.
// Proof method: Monte Carlo random points, such that the dependency is true, if
// one point gives a positive integrand.
bool OptimalDesignFeatureSelection::proof_dimension_dependency(
    LinearModel linear_model) {
  std::mt19937 gen(std::random_device{}());
  std::uniform_real_distribution<> dis(0, 1);
  int dim_size = option.bounds.x.u.size();
  VectorXd span = option.bounds.x.u - option.bounds.x.l;
  VectorXi grad_check = VectorXi::Zero(dim_size);
  int feat_size = linear_model.feature_size();
  VectorXd coeff_ones = VectorXd::Constant(feat_size, 1.0);
  linear_model.set_coeff(coeff_ones);
  bool check_dependency = false;
  int iter = 0;
  while (iter++ < option.max_proof_dependency_iter && !check_dependency) {
    VectorXd x_proof_point = option.bounds.x.l + dis(gen) * span;
    linear_model.set_x(x_proof_point);
    VectorXd abs_grad = linear_model.grad();
    for (int i = 0; i < dim_size; ++i) {
      if (std::abs(abs_grad(i)) > 0) {
        grad_check(i) = 1.0;
      }
    }
    if (grad_check.prod() > 0) {
      check_dependency = true;
    }
  }
  return check_dependency;
}
// Computing binomial coefficient
int OptimalDesignFeatureSelection::binom_coeff(int n, int k) {
  int binom_coeff = 1;
  // symmetry
  if (k > n - k)
    k = n - k;
  for (int i = 0; i < k; ++i) {
    binom_coeff *= (n - i);
    binom_coeff /= (i + 1);
  }
  return binom_coeff;
}
// Map: binomial coefficient -> unique position
int OptimalDesignFeatureSelection::binom_coeff_to_pos(VectorXi idx, int n,
                                                      int k) {
  int pos = 0;
  for (int i = 0; i < k; ++i) {
    int shift = i > 0 ? idx(i - 1) : -1;
    for (int j = 0; j < idx(i) - shift - 1; ++j) {
      pos += binom_coeff(n - idx(i) + j, k - i - 1);
    }
  }
  return pos;
}
// Create efficiently all subsets of length k of an index set of length n
void OptimalDesignFeatureSelection::all_subsets_certain_length(
    int n, int k, MatrixXi &subsets, VectorXi subset, int i) {
  int idx_start = i > 0 ? subset(i - 1) + 1 : 0;
  for (int idx = idx_start; idx < n - k + i + 1; ++idx) {
    subset(i) = idx;
    if (0 == i - k + 1) {
      int pos = binom_coeff_to_pos(subset, n, k);
      subsets.col(pos) = subset;
    } else {
      all_subsets_certain_length(n, k, subsets, subset, i + 1);
    }
  }
}
// Build all unique subsets of length m (horizon)
std::vector<LinearModel>
OptimalDesignFeatureSelection::horizon_models(LinearModel linear_model,
                                              FeatureSet feature_set) {
  int m = option.ccrit_horizon.m;
  int feat_set_size = feature_set.size();
  // Get all subsets of length m in index set of length n = feat_set_size
  MatrixXi subsets(m, binom_coeff(feat_set_size, m));
  VectorXi subset(m);
  all_subsets_certain_length(feat_set_size, m, subsets, subset, 0);
  // From index map to linear models
  std::vector<LinearModel> linear_horizon_models;
  for (int i = 0; i < subsets.cols(); ++i) {
    LinearModel linear_model_horizon = linear_model;
    VectorXi feat_ids = subsets.col(i);
    for (int j = 0; j < m; ++j) {
      int idx = feat_ids(j);
      linear_model_horizon.add_feature(feature_set[idx]);
    }
    linear_horizon_models.push_back(linear_model_horizon);
  }
  return linear_horizon_models;
}
// Simulate a oracle measurement
double OptimalDesignFeatureSelection::oracle_measurement(VectorXd x) {
  // Get expected value of oracle
  option.oracle.linear_model.set_x(x);
  double expect_value = option.oracle.linear_model.val();
  // Random error of measurement
  std::mt19937 gen(std::random_device{}());
  std::normal_distribution<double> dis(0.0, option.oracle.sigma);
  double error = dis(gen);
  double oracle_meas = expect_value + error;
  return oracle_meas;
}
// Map K_w: continuous design -> discrete design
void OptimalDesignFeatureSelection::realize_optimal_design(
    OptimalDesign optimal_design) {
  logging::info("Realize optimal design");
  int supp_size = optimal_design.weights.size();
  // number of measurements on supp vector = ceil(weight/per_step)
  VectorXi supp_meas_number(supp_size);
  int number_of_non_zero_points = 0;
  for (int i = 0; i < supp_size; ++i) {
    VectorXd x = optimal_design.supp.col(i);
    int number_x_measured = 0;
    // get number of supp vectors just measured
    for (int k = 0; k < option.measurements.data.x.cols(); ++k) {
      VectorXd supp_vector = option.measurements.data.x.col(k);
      if ((x - supp_vector).norm() < 1e-8) {
        number_x_measured++;
      }
    }
    int meas_needed = std::ceil(optimal_design.weights(i) /
                                option.measurements.meas_perc_step);
    supp_meas_number(i) = std::max(meas_needed - number_x_measured, 0);
    if (supp_meas_number(i) > 0) {
      number_of_non_zero_points++;
    }
  }
  // perform oracle measurements
  int new_measurements_number = supp_meas_number.sum();
  logging::info(
      "Realize optimal design | Realize " +
      std::to_string(new_measurements_number) + " new measurements at " +
      std::to_string(number_of_non_zero_points) + " different support points.");
  MatrixXd meas_data_x(optimal_design.supp.rows(), new_measurements_number);
  VectorXd meas_data_y(new_measurements_number);
  int pos = 0;
  for (int i = 0; i < supp_size; ++i) {
    VectorXd x = optimal_design.supp.col(i);
    for (int j = 0; j < supp_meas_number(i); ++j) {
      meas_data_x.col(pos) = x;
      meas_data_y(pos) = oracle_measurement(x);
      pos++;
    }
  }
  // append to measurements
  // x
  MatrixXd new_data_x(optimal_design.supp.rows(),
                      option.measurements.data.x.cols() +
                          new_measurements_number);
  new_data_x.block(0, 0, optimal_design.supp.rows(),
                   option.measurements.data.x.cols()) =
      option.measurements.data.x;
  new_data_x.block(0, option.measurements.data.x.cols(),
                   optimal_design.supp.rows(), new_measurements_number) =
      meas_data_x;
  // y
  VectorXd new_data_y(option.measurements.data.y.size() +
                      new_measurements_number);
  new_data_y.head(option.measurements.data.y.size()) =
      option.measurements.data.y;
  new_data_y.segment(option.measurements.data.y.size(),
                     new_measurements_number) = meas_data_y;
  // new data to data
  option.measurements.data.x = new_data_x;
  option.measurements.data.y = new_data_y;
}
// Get the best model dependend on RMSE of every model related to the data set.
int OptimalDesignFeatureSelection::argmin_rmse_linear_model_id(
    std::vector<LinearModel> &linear_models) {
  // init regression problem
  Measurements meas;
  meas.x = option.measurements.data.x;
  meas.y = option.measurements.data.y;
  LinearRegression linear_regression(meas);
  int best_model_id = 0;
  double best_model_rmse = std::numeric_limits<double>::max();
  for (int i = 0; i < linear_models.size(); ++i) {
    linear_regression.fit(linear_models[i]);
    if (linear_models[i].rmse < best_model_rmse) {
      best_model_id = i;
      best_model_rmse = linear_models[i].rmse;
    }
  }
  return best_model_id;
}
// Compare two vectors in length and values
bool OptimalDesignFeatureSelection::compare_vec(std::vector<int> a,
                                                std::vector<int> b) {
  std::sort(a.begin(), a.end());
  std::sort(b.begin(), b.end());
  if (a.size() != b.size()) {
    return false;
  }
  bool check = true;
  for (int i = 0; i < a.size(); ++i) {
    if (a[i] != b[i]) {
      check = false;
    }
  }
  return check;
}
// Simulation of optimal design feature selection
void OptimalDesignFeatureSelection::simulate(LinearModel linear_model,
                                             FeatureSet feature_set) {
  logging::info("ODFS | Start simulation.");
  // init lower cardinality bound n0 = size of linear model
  option.n0 = linear_model.feature_size();
  logging::info("ODFS:Simulation | n0 = " + std::to_string(option.n0));
  // check dimension dependency of null modell
  if (!proof_dimension_dependency(linear_model)) {
    throw std::invalid_argument(
        "odfs: dimension dependency check of null modell failed");
  }
  // cardinality must be less than total feature set (nullmodel and feature
  // set) minus m
  if (option.termination.cardinality >
      option.n0 + feature_set.size() + 1 - option.ccrit_horizon.m) {
    throw std::invalid_argument(
        "odfs: wished cardinality " +
        std::to_string(option.termination.cardinality) +
        " is greater then size of nullmodel (" + std::to_string(option.n0) +
        ") plus size of feature set (" + std::to_string(feature_set.size()) +
        ") plus 1 minus m (" + std::to_string(option.ccrit_horizon.m) + "))");
  }
  // clear result
  ODFS_Result clear_result;
  result = clear_result;
  // init linear model to best model of cardinality n0
  cardinality_linear_models.push_back(linear_model);
  // start algorithm with step forward selection
  forward_selection(linear_model, feature_set);
}
// Forward selection
void OptimalDesignFeatureSelection::forward_selection(LinearModel linear_model,
                                                      FeatureSet feature_set) {
  std::vector<int> linear_model_ids = linear_model.features_id();
  std::string linear_model_ids_string;
  linear_model_ids_string = "[" + std::to_string(linear_model_ids[0]);
  for (int k = 1; k < linear_model_ids.size(); ++k) {
    linear_model_ids_string += "; " + std::to_string(linear_model_ids[k]);
  }
  linear_model_ids_string += "]";
  std::cout << "forward start: " << linear_model_ids_string
            << ", rmse: " << std::to_string(linear_model.rmse) << ", #supp: "
            << std::to_string(option.measurements.total_supp.cols())
            << ", #meas: " << std::to_string(option.measurements.data.y.size())
            << '\n';
  logging::info("ODFS:Simulation | Start forward selection");
  int card = linear_model.feature_size();
  logging::info("ODFS:Simulation:Forward | card = " + std::to_string(card));
  /** Optimal Design of
   * ODFS Measure:
   * d-/c-crit + measure
   * costs
   */
  // design grid
  DesignMeasureGrid grid(option.bounds.x.l, option.bounds.x.u,
                         option.grid.sampling, option.measurements.total_supp);
  AdapativeGridOptimalDesignOption adaptive_grid_option;
  adaptive_grid_option.supp.minimize.bounds.x.u = option.bounds.x.u;
  adaptive_grid_option.supp.minimize.bounds.x.l = option.bounds.x.l;

  DesignOption design_option;
  design_option.design_type = "odfs-criterium";
  // d-crit linear model
  std::vector<LinearModel> design_linear_models;
  design_linear_models.push_back(linear_model);
  // c-crit linear
  // "horizon" models
  std::vector<LinearModel> horizon_linear_models =
      horizon_models(linear_model, feature_set);
  for (int i = 0; i < horizon_linear_models.size(); ++i) {
    design_linear_models.push_back(horizon_linear_models[i]);
  }
  logging::info("ODFS:Simulation:Forward | #C-crit-horizon-models = " +
                std::to_string(design_linear_models.size()));
  design_option.linear_models = design_linear_models;
  design_option.target_weights = option.target_weights.sequence[card - 1];
  design_option.c_crit_m = option.ccrit_horizon.m;
  design_option.cost_delta = option.cost.delta;
  design_option.measured_supp_ids = grid.measured_supp_ids;
  AdapativeGridOptimalDesign adaptive_grid_method(design_option,
                                                  adaptive_grid_option);
  OptimalDesign optimal_design = adaptive_grid_method.solve(grid.supp);
  // new supp to supp
  option.measurements.optimal_design.supp = optimal_design.supp;
  // save weights of optimal design
  option.measurements.optimal_design.weights = optimal_design.weights;
  optimal_design_to_total_supp(optimal_design.supp);
  /** Realization of
   * optimal design */
  realize_optimal_design(optimal_design);
  // rmse of linear model
  Measurements meas;
  meas.x = option.measurements.data.x;
  meas.y = option.measurements.data.y;
  LinearRegression linear_regression(meas);
  linear_regression.fit(linear_model);
  // build potential
  // linear models
  std::vector<LinearModel> pot_linear_models;
  for (int i = 0; i < feature_set.size(); ++i) {
    LinearModel pot_linear_model = linear_model;
    pot_linear_model.add_feature(feature_set[i]);
    pot_linear_models.push_back(pot_linear_model);
  }
  logging::info("ODFS:Simulation:Forward | #Potential linear modeals = " +
                std::to_string(pot_linear_models.size()));
  // argmin rmse of potential linear models
  int best_model_id = argmin_rmse_linear_model_id(pot_linear_models);
  // sffs parameter for trace
  SFFSParameter sffs_parameter;
  sffs_parameter.selection_type = "forward";
  sffs_parameter.pot_linear_models = pot_linear_models;
  sffs_parameter.linear_model = linear_model;
  sffs_parameter.feature_set = feature_set;
  sffs_parameter.measurements = option.measurements;
  logging::info("ODFS:Simulation:Forward | argmin rmse, best model id = " +
                std::to_string(best_model_id));
  // update linear model
  linear_model = pot_linear_models[best_model_id];
  card++;
  // remove new feature from set
  feature_set.erase(feature_set.begin() + best_model_id);
  // append sffs parameter to result trace
  result.sffs_trace.push_back(sffs_parameter);
  // set linear model to cardinality n
  if (card - option.n0 < cardinality_linear_models.size()) {
    cardinality_linear_models[card - option.n0] = linear_model;
  } else {
    cardinality_linear_models.push_back(linear_model);
  }
  // termination max support vectors
  if (option.termination.supp_number >=
      option.measurements.optimal_design.supp.cols()) {
    // go to backward selection
    backward_selection(linear_model, feature_set);
  } else {
    logging::info(
        "ODFS | Termination, max supp number (" +
        std::to_string(option.termination.supp_number) +
        ") reached, measurements supp number = " +
        std::to_string(option.measurements.optimal_design.supp.cols()));
  }
}
// Backward selection
void OptimalDesignFeatureSelection::backward_selection(LinearModel linear_model,
                                                       FeatureSet feature_set) {
  std::vector<int> linear_model_ids = linear_model.features_id();
  std::string linear_model_ids_string;
  linear_model_ids_string = "[" + std::to_string(linear_model_ids[0]);
  for (int k = 1; k < linear_model_ids.size(); ++k) {
    linear_model_ids_string += "; " + std::to_string(linear_model_ids[k]);
  }
  linear_model_ids_string += "]";
  std::cout << "backward start: " << linear_model_ids_string
            << ", rmse: " << std::to_string(linear_model.rmse) << ", #supp: "
            << std::to_string(option.measurements.total_supp.cols())
            << ", #meas: " << std::to_string(option.measurements.data.y.size())
            << '\n';
  logging::info("ODFS:Simulation | Start backward selection");
  int card = linear_model.feature_size();
  // potential linear models remove one feature
  std::vector<LinearModel> pot_linear_models;
  std::vector<int> features_id = linear_model.features_id();
  FeatureSet pot_features;
  for (int i = 0; i < features_id.size(); ++i) {
    int feat_id = features_id[i];
    LinearModel pot_linear_model = linear_model;
    LinearModelFeature del_feature = linear_model.feature(feat_id);
    pot_linear_model.delete_feature(feat_id);
    if (proof_dimension_dependency(pot_linear_model)) {
      pot_linear_models.push_back(pot_linear_model);
      pot_features.push_back(del_feature);
    }
  }
  // argmin rmse of potential linear models
  int best_model_id = argmin_rmse_linear_model_id(pot_linear_models);
  logging::info("ODFS:Simulation:Backward | argmin rmse, best model id = " +
                std::to_string(best_model_id));
  // sffs parameter for trace
  SFFSParameter sffs_parameter;
  sffs_parameter.selection_type = "backward";
  sffs_parameter.pot_linear_models = pot_linear_models;
  sffs_parameter.linear_model = linear_model;
  sffs_parameter.feature_set = feature_set;
  sffs_parameter.measurements = option.measurements;
  /** cases
   * - 1: linear_model[card-1] = best_model_id model -> forward selection
   * - 2: linear_model[card-1] != best_model_id model ->backward selection
   */
  std::vector<int> best_model_features_id =
      pot_linear_models[best_model_id].features_id();
  std::vector<int> cardinality_linear_models_id =
      cardinality_linear_models[card - option.n0 - 1].features_id();

  if (compare_vec(best_model_features_id, cardinality_linear_models_id)) {
    if (card < option.termination.cardinality) {
      logging::info("ODFS:Simulation:Backward | did not found a better model, "
                    "go to forward selection");
      // append sffs parameter to result trace
      result.sffs_trace.push_back(sffs_parameter);
      // go to forward selection
      forward_selection(linear_model, feature_set);
    } else {
      logging::info("ODFS Simulation finished.");
      // append sffs parameter to result trace
      result.sffs_trace.push_back(sffs_parameter);
      // save result
      result.linear_model = linear_model;
      result.feature_set = feature_set;
      result.measurements = option.measurements;
    }
  } else {
    // case 2
    // add removed feature to set
    feature_set.push_back(pot_features[best_model_id]);
    // overwrite linear_model of card
    linear_model = pot_linear_models[best_model_id];
    card--;
    // save linear model
    cardinality_linear_models[card - option.n0] = linear_model;
    // append sffs parameter to result trace
    result.sffs_trace.push_back(sffs_parameter);
    // go further to backward selection
    if (card > option.n0) {
      logging::info("ODFS:Simulation:Backward | found a better model, try next "
                    "backward selection");
      // go to forward selection
      backward_selection(linear_model, feature_set);
    } else {
      logging::info("ODFS:Simulation:Backward | found a better model, minimal "
                    "cardinality reached, go to forward selection");
      forward_selection(linear_model, feature_set);
    }
  }
}
// Expected RMSE := RMSE(expected value of oracle, trained linear model) on data
// set D.
double OptimalDesignFeatureSelection::expected_rmse(MatrixXd supp,
                                                    LinearModel linear_model) {
  LinearModel oracle = option.oracle.linear_model;
  double exp_rmse = 0.0;
  int data_size = supp.cols();
  for (int i = 0; i < data_size; ++i) {
    VectorXd x = supp.col(i);
    linear_model.set_x(x);
    oracle.set_x(x);
    exp_rmse += std::pow(oracle.val() - linear_model.val(), 2);
  }
  exp_rmse /= (double)data_size;
  exp_rmse = std::sqrt(exp_rmse);
  return exp_rmse;
}
// Append new support points to global set of support points.
void OptimalDesignFeatureSelection::optimal_design_to_total_supp(
    MatrixXd supp) {
  int supp_size = supp.cols();
  int total_supp_size = option.measurements.total_supp.cols();
  MatrixXd new_total_supp = option.measurements.total_supp;
  MatrixXd tmp_total_supp;
  bool append_to_total_supp;
  for (int i = 0; i < supp_size; ++i) {
    append_to_total_supp = true;
    VectorXd x = supp.col(i);
    for (int j = 0; j < total_supp_size; ++j) {
      VectorXd total_x = option.measurements.total_supp.col(j);
      if ((x - total_x).norm() < 1e-8) {
        append_to_total_supp = false;
      }
    }
    // append_to_total_supp
    if (append_to_total_supp) {
      tmp_total_supp =
          MatrixXd(new_total_supp.rows(), new_total_supp.cols() + 1);
      tmp_total_supp.block(0, 0, new_total_supp.rows(), new_total_supp.cols()) =
          new_total_supp;
      tmp_total_supp.col(new_total_supp.cols()) = x;
      new_total_supp = tmp_total_supp;
    }
  }
  // new total supp to total supp
  option.measurements.total_supp = new_total_supp;
}
// Export the result of simulation
void OptimalDesignFeatureSelection::export_result() {
  logging::info("ODFS export results");
  std::string simulation_id = option.simulation_id;
  std::string folder_id = option.folder_id;
  /** criteria weights
   * - columns: cardinality, d-crit weight, c-crit weight, cost-crit weight
   */
  int criteria_weights_size = option.target_weights.sequence.size();
  std::vector<std::string> criteria_weights{
      "cardinality", "d-crit weight", "c-crit weight", "cost-crit weight"};
  for (int i = 0; i < criteria_weights_size; ++i) {
    // cardinality
    criteria_weights.push_back(std::to_string(1 + i));
    // det-crit weight
    for (int k = 0; k < 3; ++k) {
      criteria_weights.push_back(
          to_string_with_precision(option.target_weights.sequence[i][k]));
    }
  }
  // export criteria weights
  export_table(simulation_id, folder_id, "criteria_weights", criteria_weights,
               4);
  logging::info("ODFS 'criteria weights' exported");
  /** sffs trace
   * - columns: iteration, selection type, base lin model feauture ids, base
   * det efficiency, rmse
   */
  int sffs_trace_size = result.sffs_trace.size();
  std::vector<std::string> sffs_trace{
      "sffs_iteration",       "selection_type",     "linear_model_feature_ids",
      "cardinality",          "det_efficiency",     "estimated_rmse",
      "expected_rmse",        "total_measurements", "total_support_vectors",
      "local_support_vectors"};
  int sffs_trace_cols = sffs_trace.size();
  VectorXd sffs_trace_x_sampling = VectorXd::Constant(
      option.grid.sampling.size(), option.export_option.sampling_per_dim);

  DesignMeasureGrid sffs_trace_x_grid(option.bounds.x.l, option.bounds.x.u,
                                      sffs_trace_x_sampling);
  MatrixXd sffs_trace_x_grid_supp = sffs_trace_x_grid.supp;

  for (int i = 0; i < sffs_trace_size; ++i) {
    SFFSParameter par = result.sffs_trace[i];
    // iteration
    sffs_trace.push_back(std::to_string(i));
    // selection type
    sffs_trace.push_back(par.selection_type);
    // linear model ids [id_0,id_1,..]
    std::vector<int> linear_model_ids = par.linear_model.features_id();
    std::string linear_model_ids_string;
    linear_model_ids_string = "[" + std::to_string(linear_model_ids[0]);
    for (int k = 1; k < linear_model_ids.size(); ++k) {
      linear_model_ids_string += "; " + std::to_string(linear_model_ids[k]);
    }
    linear_model_ids_string += "]";
    sffs_trace.push_back(linear_model_ids_string);
    // linear model cardinality
    sffs_trace.push_back(std::to_string(linear_model_ids.size()));
    // determinant efficiency
    LinearModel linear_model = par.linear_model;
    DesignEfficiency design_eff(par.measurements.optimal_design.supp,
                                par.measurements.optimal_design.weights,
                                linear_model, option.bounds.x.l,
                                option.bounds.x.u, option.grid.sampling);
    // add det eff
    sffs_trace.push_back(to_string_with_precision(design_eff.d_crit()));
    // add estimated rmse
    sffs_trace.push_back(to_string_with_precision(linear_model.rmse));
    // add expected rmse
    double exp_rmse = expected_rmse(sffs_trace_x_grid_supp, linear_model);
    sffs_trace.push_back(to_string_with_precision(exp_rmse));
    // total number of measurements
    int total_measurements = par.measurements.data.y.size();
    sffs_trace.push_back(std::to_string(total_measurements));
    // total support vectors
    int total_support_vectors = par.measurements.total_supp.size();
    sffs_trace.push_back(std::to_string(total_support_vectors));
    // local support vectors
    int local_support_vectors = par.measurements.optimal_design.supp.cols();
    sffs_trace.push_back(std::to_string(local_support_vectors));
  }
  // export criteria weights
  export_table(simulation_id, folder_id, "sffs_trace", sffs_trace,
               sffs_trace_cols);
  logging::info("ODFS 'sffs trace' exported");

  /** Optimal Designs
   * - weight, supp dim 0, supp dim 1, ....
   */
  int supp_dim =
      result.sffs_trace[0].measurements.optimal_design.supp.col(0).size();
  int optimal_designs_cols = 1 + supp_dim;
  std::vector<std::string> optimal_design_weights_title{"weights"};
  for (int i = 0; i < supp_dim; ++i) {
    optimal_design_weights_title.push_back("x_" + std::to_string(i));
  }
  for (int i = 0; i < sffs_trace_size; ++i) {
    if ("forward" == result.sffs_trace[i].selection_type) {
      OptimalDesign optimal_design =
          result.sffs_trace[i].measurements.optimal_design;
      VectorXd weights = optimal_design.weights;
      MatrixXd supp = optimal_design.supp;
      // write string
      std::vector<std::string> optimal_design_weights =
          optimal_design_weights_title;
      for (int j = 0; j < weights.size(); ++j) {
        optimal_design_weights.push_back(to_string_with_precision(weights(j)));
        VectorXd supp_vec = supp.col(j);
        for (int n = 0; n < supp_vec.size(); ++n) {
          optimal_design_weights.push_back(std::to_string(supp_vec(n)));
        }
      }
      export_table(simulation_id, folder_id,
                   "optimal_design_sffs_iteration_" + std::to_string(i),
                   optimal_design_weights, optimal_designs_cols);
    }
  }
  /** Measurements
   * - x_0, x_1, ..., y
   */
  int measurements_cols = 1 + supp_dim;
  std::vector<std::string> sffs_measurements_title{"x_0"};
  for (int i = 1; i < supp_dim; ++i) {
    sffs_measurements_title.push_back("x_" + std::to_string(i));
  }
  sffs_measurements_title.push_back("y");
  for (int i = 0; i < sffs_trace_size; ++i) {
    VectorXd y = result.sffs_trace[i].measurements.data.y;
    MatrixXd x = result.sffs_trace[i].measurements.data.x;
    // write string
    std::vector<std::string> sffs_measurements = sffs_measurements_title;
    for (int j = 0; j < y.size(); ++j) {
      VectorXd x_vec = x.col(j);
      for (int n = 0; n < x_vec.size(); ++n) {
        sffs_measurements.push_back(std::to_string(x_vec(n)));
      }
      sffs_measurements.push_back(std::to_string(y(j)));
    }
    export_table(simulation_id, folder_id,
                 "measurements_sffs_iteration_" + std::to_string(i),
                 sffs_measurements, measurements_cols);
  }
  logging::info("ODFS 'measurements' exported");
  /** Linear Models
   * - y, x
   */
  VectorXd x_sampling = VectorXd::Constant(
      option.grid.sampling.size(), option.export_option.sampling_per_dim);

  DesignMeasureGrid linear_models_x_grid(option.bounds.x.l, option.bounds.x.u,
                                         x_sampling);
  MatrixXd x_grid = linear_models_x_grid.supp;
  std::vector<std::string> sffs_linear_model_title{"x_0"};
  for (int i = 1; i < supp_dim; ++i) {
    sffs_linear_model_title.push_back("x_" + std::to_string(i));
  }
  sffs_linear_model_title.push_back("y_oracle");
  for (int i = 0; i < sffs_trace_size; ++i) {
    std::vector<LinearModel> sffs_pot_linear_models =
        result.sffs_trace[i].pot_linear_models;
    std::vector<std::string> pot_linear_models_samples =
        sffs_linear_model_title;
    auto base_linear_model_ids =
        result.sffs_trace[i].linear_model.features_id();
    std::string base_linear_model_ids_string =
        "y_[" + std::to_string(base_linear_model_ids[0]);
    for (int k = 1; k < base_linear_model_ids.size(); ++k) {
      base_linear_model_ids_string +=
          "; " + std::to_string(base_linear_model_ids[k]);
    }
    base_linear_model_ids_string += "]";
    pot_linear_models_samples.push_back(base_linear_model_ids_string);
    for (int j = 0; j < sffs_pot_linear_models.size(); ++j) {
      auto linear_model_ids = sffs_pot_linear_models[j].features_id();
      std::string linear_model_ids_string =
          "y_[" + std::to_string(linear_model_ids[0]);
      for (int k = 1; k < linear_model_ids.size(); ++k) {
        linear_model_ids_string += "; " + std::to_string(linear_model_ids[k]);
      }
      linear_model_ids_string += "]";
      pot_linear_models_samples.push_back(linear_model_ids_string);
    }
    int csv_cols = pot_linear_models_samples.size();
    for (int x_grid_i = 0; x_grid_i < x_grid.cols(); ++x_grid_i) {
      VectorXd x_vec = x_grid.col(x_grid_i);
      for (int x_dim = 0; x_dim < x_vec.size(); ++x_dim) {
        pot_linear_models_samples.push_back(std::to_string(x_vec(x_dim)));
      }
      // oracle
      option.oracle.linear_model.set_x(x_vec);
      pot_linear_models_samples.push_back(
          to_string_with_precision(option.oracle.linear_model.val()));
      // base linear model
      LinearModel base_linear_model = result.sffs_trace[i].linear_model;
      base_linear_model.set_x(x_vec);
      pot_linear_models_samples.push_back(
          to_string_with_precision(base_linear_model.val()));
      // potential linear models
      for (int j = 0; j < sffs_pot_linear_models.size(); ++j) {
        LinearModel linear_model = sffs_pot_linear_models[j];
        linear_model.set_x(x_vec);
        double linear_model_val = linear_model.val();
        pot_linear_models_samples.push_back(
            to_string_with_precision(linear_model_val));
      }
    }
    export_table(simulation_id, folder_id,
                 "data_sffs_iteration_" + std::to_string(i),
                 pot_linear_models_samples, csv_cols);
  }
  logging::info("ODFS 'data' exported");
  /** General ODFS Algorithm Options
   * row-wise options
   */
  std::vector<std::string> odfs_algorithm_details;
  // simulation id
  odfs_algorithm_details.push_back("simulation id");
  odfs_algorithm_details.push_back(option.simulation_id);
  // x-bounds : upper
  odfs_algorithm_details.push_back("upper bound x");
  std::string upper_bound_x = "[" + std::to_string(option.bounds.x.u(0));
  for (int i = 1; i < option.bounds.x.u.size(); ++i) {
    upper_bound_x += "; " + std::to_string(option.bounds.x.u(i));
  }
  upper_bound_x += "]";
  odfs_algorithm_details.push_back(upper_bound_x);
  // x-bounds: lower
  odfs_algorithm_details.push_back("lower bound x");
  std::string lower_bound_x = "[ " + std::to_string(option.bounds.x.l(0));
  for (int i = 1; i < option.bounds.x.l.size(); ++i) {
    lower_bound_x += "; " + std::to_string(option.bounds.x.l(i));
  }
  lower_bound_x += "]";
  odfs_algorithm_details.push_back(lower_bound_x);
  // adaptive grid sampling
  odfs_algorithm_details.push_back("adaptive grid sampling");
  std::string adaptive_grid_sampling =
      "[ " + std::to_string(option.grid.sampling(0));
  for (int i = 1; i < option.grid.sampling.size(); ++i) {
    adaptive_grid_sampling +=
        "; " + std::to_string((int)option.grid.sampling(i));
  }
  adaptive_grid_sampling += "]";
  odfs_algorithm_details.push_back(adaptive_grid_sampling);
  // max proof dependency iterations
  odfs_algorithm_details.push_back("max proof dependency iterations");
  odfs_algorithm_details.push_back(
      std::to_string(option.max_proof_dependency_iter));
  // target weights
  odfs_algorithm_details.push_back("target weights type");
  odfs_algorithm_details.push_back(option.target_weights.type);
  odfs_algorithm_details.push_back("target weights det weight min");
  odfs_algorithm_details.push_back(
      std::to_string(option.target_weights.det_weight_min));
  odfs_algorithm_details.push_back("target weights det weight max");
  odfs_algorithm_details.push_back(
      std::to_string(option.target_weights.det_weight_max));
  odfs_algorithm_details.push_back("target weight measure cost");
  odfs_algorithm_details.push_back(
      std::to_string(option.target_weights.weight_measure_cost));
  // termination
  odfs_algorithm_details.push_back("termination cardinality");
  odfs_algorithm_details.push_back(
      std::to_string(option.termination.cardinality));
  odfs_algorithm_details.push_back("termination supp vectors");
  odfs_algorithm_details.push_back(
      std::to_string(option.termination.supp_number));
  // cost
  odfs_algorithm_details.push_back("cost type");
  odfs_algorithm_details.push_back(option.cost.type);
  odfs_algorithm_details.push_back("cost delta");
  odfs_algorithm_details.push_back(std::to_string(option.cost.delta));
  // ccrit horizon
  odfs_algorithm_details.push_back("c-crit m");
  odfs_algorithm_details.push_back(std::to_string(option.ccrit_horizon.m));
  // nullmodel size
  odfs_algorithm_details.push_back("null model size");
  odfs_algorithm_details.push_back(std::to_string(option.n0));
  // oracle
  odfs_algorithm_details.push_back("oracle sigma");
  odfs_algorithm_details.push_back(std::to_string(option.oracle.sigma));
  // export sampling per dim
  odfs_algorithm_details.push_back("export option sampling per dim");
  odfs_algorithm_details.push_back(
      std::to_string(option.export_option.sampling_per_dim));
  // linear model nullmodel and feature set
  odfs_algorithm_details.push_back("nullmodel feature ids");
  std::string null_model_feature_ids =
      "[ " + std::to_string(result.sffs_trace[0].linear_model.features_id()[0]);
  for (int i = 1; i < result.sffs_trace[0].linear_model.feature_size(); ++i) {
    null_model_feature_ids +=
        "; " +
        std::to_string(result.sffs_trace[0].linear_model.features_id()[i]);
  }
  null_model_feature_ids += "]";
  odfs_algorithm_details.push_back(null_model_feature_ids);
  odfs_algorithm_details.push_back("feature set");
  std::string feature_set_ids =
      "[ " + std::to_string(result.sffs_trace[0].feature_set[0].id);
  for (int i = 1; i < result.sffs_trace[0].feature_set.size(); ++i) {
    feature_set_ids +=
        "; " + std::to_string(result.sffs_trace[0].feature_set[i].id);
  }
  feature_set_ids += "]";
  odfs_algorithm_details.push_back(feature_set_ids);
  // oracle feature ids
  odfs_algorithm_details.push_back("oracle feature ids");
  std::string oracle_feature_ids =
      "[ " + std::to_string(option.oracle.linear_model.features_id()[0]);
  for (int i = 1; i < option.oracle.linear_model.feature_size(); ++i) {
    oracle_feature_ids +=
        "; " + std::to_string(option.oracle.linear_model.features_id()[i]);
  }
  oracle_feature_ids += "]";
  odfs_algorithm_details.push_back(oracle_feature_ids);
  // final model feature ids
  odfs_algorithm_details.push_back("final model feature ids");
  std::vector<int> final_ids = result.linear_model.features_id();
  std::sort(final_ids.begin(), final_ids.end());
  std::string final_model_feature_ids = "[ " + std::to_string(final_ids[0]);
  for (int i = 1; i < final_ids.size(); ++i) {
    final_model_feature_ids += "; " + std::to_string(final_ids[i]);
  }
  final_model_feature_ids += "]";
  odfs_algorithm_details.push_back(final_model_feature_ids);
  // export adaptive grid sampling
  export_table(simulation_id, folder_id, "odfs_algorithm_overview",
               odfs_algorithm_details, 2);
  logging::info("ODFS 'odfs_algorithm_overview' exported");
  logging::info("ODFS sffs_trace exported");
}
